package com.example.projectdoc.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.example.projectdoc.network.ResultWrapper
import com.example.projectdoc.network.requests.LoginRequest
import com.example.projectdoc.repo.ProfileRepository
import com.example.projectdoc.util.Status
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: ProfileRepository) : ViewModel() {
    private val TAG = "LoginViewModelLogcat"

    private val _status = MutableLiveData<Status<Boolean>>()
    val status: LiveData<Status<Boolean>> = _status

    fun login(number: String, password: String) = viewModelScope.launch {
        val loginRequest = LoginRequest(number, password)
        val response = repository.login(loginRequest)
        Log.d(TAG, "login: $response")
        when (response) {
            is ResultWrapper.NetworkError -> _status.value = Status.Failure("Problems with network")
            is ResultWrapper.HttpError -> { _status.value = Status.Failure("Error ${response.code}") }
            is ResultWrapper.GenericError -> { _status.value = Status.Failure(response.message) }
            is ResultWrapper.Success -> {
                repository.saveToDb(response.value)
                _status.value = Status.Complete(true) // TODO doctor
            }
        }
    }

    fun post(postId: Int?) {
        viewModelScope.launch {
            when (val response =
                if (postId != null) repository.post(postId) else repository.posts()) {
                is ResultWrapper.NetworkError -> Log.d(TAG, "Network error")
                is ResultWrapper.HttpError -> Log.d(TAG, "Http error: ${response.code}")
                is ResultWrapper.GenericError -> Log.d(TAG, "generic error: ${response.message}")
                is ResultWrapper.Success -> Log.d(TAG, "post: ${response.value}")
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(private val repository: ProfileRepository) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(repository) as T
    }
}