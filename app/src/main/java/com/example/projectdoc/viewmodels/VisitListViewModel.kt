package com.example.projectdoc.viewmodels

import android.util.Log
import androidx.lifecycle.*
import com.example.projectdoc.model.VisitPage
import com.example.projectdoc.network.ResultWrapper
import com.example.projectdoc.repo.VisitRepository
import com.example.projectdoc.util.PageParams
import com.example.projectdoc.util.Status
import kotlinx.coroutines.launch

class VisitListViewModel(private val repository: VisitRepository, private val pageParams: PageParams) : ViewModel() {

    private val TAG = "VisitListVMLogcat"

    val status: LiveData<Status<VisitPage>> by lazy {
        val liveData = MutableLiveData<Status<VisitPage>>(Status.Idle)
        viewModelScope.launch {
            liveData.value = Status.Loading
            val response = repository.getVisitsList(pageParams)
            Log.d(TAG, "getVisitList: $response")
            when (response) {
                is ResultWrapper.NetworkError -> liveData.value = Status.Failure("Problems with network")
                is ResultWrapper.HttpError -> liveData.value = Status.Failure("Error ${response.code}")
                is ResultWrapper.GenericError -> liveData.value = Status.Failure(response.message)
                is ResultWrapper.Success -> liveData.value = Status.Complete(response.value)
            }
        }
        return@lazy liveData
    }
}

@Suppress("UNCHECKED_CAST")
class VisitListFactory(
    private val repository: VisitRepository,
    private val pageParams: PageParams
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return VisitListViewModel(repository, pageParams) as T
    }
}