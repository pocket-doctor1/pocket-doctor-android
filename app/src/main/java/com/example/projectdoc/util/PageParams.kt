package com.example.projectdoc.util

data class PageParams(val pageNumber: Int, val history: Int, val active: Int)