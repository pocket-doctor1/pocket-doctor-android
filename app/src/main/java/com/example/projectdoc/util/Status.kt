package com.example.projectdoc.util

sealed class Status<out T> {
    data class Complete<out T>(val result: T) : Status<T>()
    data class Failure(val message: String) : Status<Nothing>()
    object Loading : Status<Nothing>()
    object Idle : Status<Nothing>()
}