package com.example.projectdoc.repo

import com.example.projectdoc.network.ApiFactory
import com.example.projectdoc.util.PageParams
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class VisitRepository(private val dispatcher: CoroutineDispatcher = Dispatchers.IO) {
    private val remoteApi = ApiFactory.apiService

    suspend fun getVisitsList(pageParams: PageParams) = safeApiCall(dispatcher) {
        remoteApi.getVisitList(4, pageParams.pageNumber * 4, pageParams.history, pageParams.active)
    }
}