package com.example.projectdoc.repo

import android.util.Log
import com.example.projectdoc.network.ResultWrapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

private val TAG = "Logcat"

suspend fun <T> safeApiCall(dispatcher: CoroutineDispatcher, call: suspend () -> T): ResultWrapper<T> {
    return withContext(dispatcher) {
        try {
            ResultWrapper.Success(call.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> ResultWrapper.NetworkError
                is HttpException -> {
                    val code = throwable.code()
                    Log.d(TAG, "safeApiCall: message: ${throwable.response()?.message()}")
                    Log.d(TAG, "safeApiCall: error body string: ${throwable.response()?.errorBody()?.string()}")
                    ResultWrapper.HttpError(code)
                }
                else -> ResultWrapper.GenericError(throwable.message ?: "Unknown error")
            }
        }
    }
}
