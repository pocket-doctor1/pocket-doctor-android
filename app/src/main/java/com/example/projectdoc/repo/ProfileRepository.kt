package com.example.projectdoc.repo

import android.content.Context
import com.example.projectdoc.model.AppDatabase
import com.example.projectdoc.model.tables.Profile
import com.example.projectdoc.network.ApiFactory
import com.example.projectdoc.network.requests.LoginRequest
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class ProfileRepository(context: Context, private val dispatcher: CoroutineDispatcher = Dispatchers.IO) {
    private val remoteApi = ApiFactory.apiService
    private val localDatabase = AppDatabase.getInstance(context.applicationContext).profileDao

    suspend fun login(request: LoginRequest) = safeApiCall(dispatcher) { remoteApi.login(request) }

    suspend fun posts() = safeApiCall(dispatcher) { remoteApi.posts() }

    suspend fun post(postId: Int) = safeApiCall(dispatcher) { remoteApi.post(postId) }

    suspend fun saveToDb(profile: Profile) = localDatabase.createOrUpdate(profile)
}