package com.example.projectdoc.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.projectdoc.R
import com.example.projectdoc.viewmodels.CurrentVisitViewModel

class CurrentVisitFragment : Fragment() {

    private lateinit var viewModel: CurrentVisitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.current_visit_fragment, container, false)
    }
}
