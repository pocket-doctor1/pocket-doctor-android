package com.example.projectdoc.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.projectdoc.R
import com.example.projectdoc.repo.VisitRepository
import com.example.projectdoc.ui.fragments.CurrentVisitFragment
import com.example.projectdoc.ui.fragments.ProfileFragment
import com.example.projectdoc.ui.fragments.VisitsHistoryFragment
import com.example.projectdoc.util.PageParams
import com.example.projectdoc.util.Status
import com.example.projectdoc.viewmodels.VisitListFactory
import com.example.projectdoc.viewmodels.VisitListViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mViewModel: VisitListViewModel

    private var profileFragment = ProfileFragment()
    private var currentVisitFragment = CurrentVisitFragment()
    private var visitsHistoryFragment = VisitsHistoryFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val factory = VisitListFactory(VisitRepository(), PageParams(0, 0, 1))
        mViewModel = ViewModelProvider(this, factory).get(VisitListViewModel::class.java)


        mViewModel.status.observe(this) {
            when (it) {
                is Status.Complete -> {
                    if (it.result.visits.isNotEmpty()) {
                        startActivity(Intent()) // TODO detailed
                    }
                    initViews()
                }
                is Status.Failure -> Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initViews() {
        view_pager.adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount() = 3

            override fun createFragment(position: Int) = when (position) {
                0 -> currentVisitFragment
                1 -> visitsHistoryFragment
                2 -> profileFragment
                else -> throw IndexOutOfBoundsException("There are only 3 fragments")
            }
        }
        TabLayoutMediator(tab_layout, view_pager) { tab: TabLayout.Tab, i: Int ->
            tab.text = resources.getStringArray(R.array.tab_titles)[i]
        }.attach()
    }
}
