package com.example.projectdoc.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.projectdoc.R
import com.example.projectdoc.viewmodels.VisitsHistoryViewModel

class VisitsHistoryFragment : Fragment() {

    private lateinit var viewModel: VisitsHistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.visits_history_fragment, container, false)
    }
}
