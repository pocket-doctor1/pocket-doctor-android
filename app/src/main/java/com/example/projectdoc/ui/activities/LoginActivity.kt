package com.example.projectdoc.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.example.projectdoc.R
import com.example.projectdoc.repo.ProfileRepository
import com.example.projectdoc.util.Status
import com.example.projectdoc.viewmodels.LoginViewModel
import com.example.projectdoc.viewmodels.LoginViewModelFactory
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var mViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val factory = LoginViewModelFactory(ProfileRepository(applicationContext))
        mViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)

        btn_login.setOnClickListener {
            val number = et_phone_number.text.toString()
            val password = et_password.text.toString()
            if (number.isEmpty()) {
                et_phone_number.error = getString(R.string.missing_number)
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                et_password.error = getString(R.string.missing_password)
                return@setOnClickListener
            }
            mViewModel.login(number, password)
//            mViewModel.post(et_phone_number.text.toString().toIntOrNull())
        }

        mViewModel.status.observe(this) {
            when(it) {
                is Status.Failure -> Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                is Status.Complete -> if (it.result) onLogin()
            }
        }
    }

    private fun onLogin() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
