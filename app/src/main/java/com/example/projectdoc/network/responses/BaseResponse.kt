package com.example.projectdoc.network.responses

import com.squareup.moshi.Json

open class BaseResponse<T> {
    @field:Json(name = "code")
    val code: Int = -1
    @field:Json(name = "message")
    val message: String = ""
    @field:Json(name = "body")
    val body: T? = null
}