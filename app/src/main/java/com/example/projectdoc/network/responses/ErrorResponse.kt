package com.example.projectdoc.network.responses

data class ErrorResponse(val description: String, val causes: Map<String, String> = emptyMap())