package com.example.projectdoc.network.requests

import com.squareup.moshi.Json


data class LoginRequest (
    @field:Json(name = "phone_number")
    var phoneNumber: String,
    var token: String,
    var doctor: Int = 1
)