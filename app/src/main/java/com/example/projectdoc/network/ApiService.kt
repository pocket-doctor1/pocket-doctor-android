package com.example.projectdoc.network

import com.example.projectdoc.LOGIN_URL
import com.example.projectdoc.POSTS_URL
import com.example.projectdoc.VISIT_LIST_URL
import com.example.projectdoc.model.VisitPage
import com.example.projectdoc.model.tables.Post
import com.example.projectdoc.model.tables.Profile
import com.example.projectdoc.network.requests.LoginRequest
import retrofit2.http.*

interface ApiService {
    @POST(LOGIN_URL)
    suspend fun login(@Body request: LoginRequest): Profile

    @GET(POSTS_URL)
    suspend fun posts(): List<Post>

    @GET("$POSTS_URL/{postId}")
    suspend fun post(@Path("postId") postId: Int): Post

    @GET(VISIT_LIST_URL)
    suspend fun getVisitList(@Query("limit") limit: Int, @Query("offset") offset: Int,
                             @Query("history") history: Int, @Query("active") active: Int) : VisitPage
}