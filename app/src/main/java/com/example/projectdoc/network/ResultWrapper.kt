package com.example.projectdoc.network

sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    data class HttpError(val code: Int? = null) : ResultWrapper<Nothing>()
    data class GenericError(val message: String = "") : ResultWrapper<Nothing>()
    object NetworkError : ResultWrapper<Nothing>()
}