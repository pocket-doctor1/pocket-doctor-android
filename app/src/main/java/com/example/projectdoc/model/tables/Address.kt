package com.example.projectdoc.model.tables

import androidx.room.*
import com.squareup.moshi.Json

@Entity(foreignKeys = [ForeignKey(entity = Profile::class, parentColumns = ["id"], childColumns = ["user_id"])])
data class Address(
    @PrimaryKey
    var id: Int = -1,
    @ColumnInfo(name = "user_id")
    @field:Json(name = "user_id")
    var profileId: Int = -1,
    var city: String = "",
    var street: String = "",
    var house: String = "",
    var corpus: String = "",
    var block: String = "",
    var floor: Int = -1,
    var flat: Int = -1,
    @ColumnInfo(name = "code")
    @field:Json(name = "code")
    var doorCode: String = "",
    var latitude: Double = -1.0,
    var longitude: Double = -1.0
)

data class VisitsToAddress(
    @Embedded
    var address: Address,
    @Relation(parentColumn = "id", entityColumn = "address_id", entity = Visit::class)
    var visits: List<Visit>
)