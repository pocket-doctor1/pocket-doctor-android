package com.example.projectdoc.model.tables

import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class Medcard(
    @PrimaryKey
    var id: Int = -1,
    @ColumnInfo(name = "first_name")
    @field:Json(name = "first_name")
    var firstName: String = "",
    @ColumnInfo(name = "last_name")
    @field:Json(name = "last_name")
    var lastName: String = "",
    @ColumnInfo(name = "middle_name")
    @field:Json(name = "middle_name")
    var middleName: String = "",
    var birthday: String = "",
    var gender: Int = -1,
    @ColumnInfo(name = "user_id")
    @field:Json(name = "user_id")
    var userId: Int = -1,
    var patronomic: String = "",
    var height: Int = -1,
    var weight: Int = -1,
    @field:Json(name = "hronics")
    var chronic: String = ""
)

data class MedcardProfile(
    @Embedded
    val profile: Profile,
    @Relation(parentColumn = "id", entityColumn = "user_id")
    val medcard: Medcard
)