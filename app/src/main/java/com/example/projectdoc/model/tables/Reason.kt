package com.example.projectdoc.model.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "reasons")
@JsonClass(generateAdapter = true)
data class Reason(
    var id: Int = -1,
    var role: String = "",
    @ColumnInfo(name = "reason")
    @field:Json(name = "value")
    var reasonTitle: String = ""
)