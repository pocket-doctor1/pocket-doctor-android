package com.example.projectdoc.model.tables

import androidx.room.*
import com.squareup.moshi.Json

@Entity
data class Doctor(
    @PrimaryKey
    var id: Int = -1,
    @ColumnInfo(name = "user_id")
    @field:Json(name = "user_id")
    var userId: Int = -1,
    @ColumnInfo(name = "clinic_id")
    @field:Json(name = "clinic_id")
    var clinicId: Int = -1,
    var role: String = "",
    var description: String = "",
    var latitude: Double = -1.0,
    var longitude: Double = -1.0
)

data class DoctorProfile(
    @Embedded
    val profile: Profile,
    @Relation(parentColumn = "id", entityColumn = "userId")
    val doctor: Doctor
)