package com.example.projectdoc.model.tables

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class Clinic(
    @PrimaryKey
    var id: Int = -1,
    var name: String = ""
)