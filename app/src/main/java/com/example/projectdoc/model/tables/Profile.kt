package com.example.projectdoc.model.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity
data class Profile(
    @field:Json(name = "phone_number")
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String = "",
    var email: String = "",
    @PrimaryKey(autoGenerate = true)
    var id: Int = -1,
    @field:Json(name = "card_id")
    @ColumnInfo(name = "card_id")
    var cardId: Int = -1,
    @field:Json(name = "doctor_id")
    @ColumnInfo(name = "doctor_id")
    var doctorId: Int = -1,
    @ColumnInfo(name = "auth_status")
    var loggedIn: Boolean = false
    // TODO medcard doctor
)