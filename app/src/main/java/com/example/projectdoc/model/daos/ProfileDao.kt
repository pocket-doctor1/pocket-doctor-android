package com.example.projectdoc.model.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.projectdoc.model.tables.Profile

@Dao
interface ProfileDao {
    @Query("SELECT * FROM profile WHERE id=:id")
    fun getProfile(id: Int): Profile

    @Query("SELECT * FROM profile WHERE auth_status = 1 limit 1")
    fun getCurrentProfile(): Profile

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun createOrUpdate(profile: Profile)
}