package com.example.projectdoc.model.tables

import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "visits", foreignKeys = [ForeignKey(
        entity = Address::class,
        parentColumns = ["id"],
        childColumns = ["address_id"],
        onDelete = ForeignKey.SET_DEFAULT
    )]
)
@JsonClass(generateAdapter = true)
data class Visit(
    var id: Int = -1,
    var role: String = "",
    @ColumnInfo(name = "address_id")
    @field:Json(name = "address_id")
    var addressId: Int = -1,
    @ColumnInfo(name = "payment_card_id")
    @field:Json(name = "payment_card_id")
    var cardId: String = "",
    @ColumnInfo(name = "doctor_id")
    @field:Json(name = "doctor_id")
    var doctorId: Int = -1,
    @ColumnInfo(name = "visit_at")
    @field:Json(name = "visit_at")
    var date: String = "",
    @ColumnInfo(name = "finished_at")
    @field:Json(name = "finished_at")
    var finishedAt: String = "",
    var questions: List<Reason> = emptyList(),
    var price: Double = -1.0
)

data class VisitDoctor(
    @Embedded
    val doctor: Doctor,
    @Relation(parentColumn = "id", entityColumn = "doctor_id")
    val visit: Visit
)

// TODO visit medcard